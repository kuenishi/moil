open Unix;;

type flag = READABLE | WRITABLE | ERROR (* with sexp, bin_io *)

external create : unit -> file_descr = "my_kqueue"
external add :     pfd:file_descr -> fd:file_descr -> flag array -> unit = "my_ev_add"
external modify :  pfd:file_descr -> fd:file_descr -> flag array -> unit = "my_ev_mod"
external del :     pfd:file_descr -> fd:file_descr -> flag array -> unit = "my_ev_del"
external oneshot : pfd:file_descr -> fd:file_descr -> flag array -> unit = "my_ev_oneshot"

external wait : pfd:file_descr -> maxevents:int -> timeout:int -> (file_descr * flag) array = "my_kevent"

external hello : string -> unit = "hello"
external env : unit -> string = "env"
