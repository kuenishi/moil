#include <caml/mlvalues.h>
#include <caml/memory.h>
//#include <caml/threads.h>

#define Nothing ((value) 0)

#include <caml/alloc.h>
#include <caml/fail.h>

#include <stdio.h>
/*
#ifndef __APPLE__
#error "use MacOSX. *BSD and Linux is in progress"
#endif */
//__GNUC__ 4
//__amd64 1
//__x86_64 1
//__x86_64__ 1

#ifdef USE_KEVENT
#include <sys/event.h>
#include <sys/types.h>
#include <sys/time.h>
#elif USE_EPOLL
#include <sys/epoll.h>
#else
#error "kevent(2) or epoll(7) should be available"
#endif

#define CAML_READABLE (0)
#define CAML_WRITABLE (1)
#define CAML_ERROR (2)

#define NS2MS (1000000)
#define NS2S  (1000000000)

#define PIN \
  printf("%s:%d entered into %s\n", __FILE__, __LINE__, __func__ );
#define POUT \
  printf("%s:%d get out from %s\n", __FILE__, __LINE__, __func__ );
#define HERE(x) \
  printf("%s:%d in %s -> %p\n", __FILE__, __LINE__, __func__, x );

#ifdef USE_KEVENT
#define READABLE EVFILT_READ
#define WRITABLE EVFILT_WRITE
#define ADD EV_ADD
#define DEL EV_DELETE
#define MOD EV_ADD
#define SET_EVENT(e,fd, ev, op)			\
  EV_SET((e), Int_val(fd), (ev), op, 0, 0, NULL)
#elif USE_EPOLL
#define READABLE EPOLLIN
#define WRITABLE EPOLLOUT
#define ADD EPOLL_CTL_ADD
#define DEL EPOLL_CTL_DEL
#define MOD EPOLL_CTL_MOD
#define ONESHOT 
#define SET_EVENT(e,fd, ev, op)			\
  { (e)->data.fd = fd; (e)->events = ev; }
#endif

CAMLprim value hello(value arg){
  CAMLparam1(arg);
  printf("hello %s!\n", String_val(arg));
  CAMLreturn(Val_unit);
}

CAMLprim value env(void){
  CAMLparam0();
  CAMLreturn(caml_copy_string(__VERSION__));
}

CAMLprim value my_kqueue(void){
#ifdef USE_KEVENT
  int kfd = kqueue();
#elif USE_EPOLL
  int kfd = epoll_create(1024);
#endif
  return Val_int(kfd);
}

CAMLprim value my_kevent(value pfd, value v_maxevents, value v_timeout){
  CAMLparam0();
  CAMLlocal2(v_res, v_flags);
  //  PIN;
  int maxevents = Int_val(v_maxevents);
  int i;

  if (maxevents <= 0) caml_invalid_argument("my_kevent: maxevents <= 0");

#ifdef USE_KEVENT
  struct timespec to;
  struct timespec * top;
  int t;
  if(t = Int_val(v_timeout)){
    top = &to;
    to.tv_sec = (t * NS2MS) / NS2S;
    to.tv_nsec = (t * NS2MS) % NS2S;
  }else{
    top = NULL;
  }
  struct kevent * evs;
  evs = (struct kevent*)caml_stat_alloc(maxevents * sizeof(struct kevent));
#elif USE_EPOLL
  struct epoll_event * evs;
  evs = (struct epoll_event*)caml_stat_alloc(maxevents * sizeof(struct epoll_event));
#endif

  caml_enter_blocking_section();
#ifdef USE_KEVENT
  i = kevent(Int_val(pfd), NULL, 0, evs, maxevents, top);
#elif USE_EPOLL
  i = epoll_wait(Int_val(pfd), evs, maxevents, Int_val(v_timeout));
#endif
  caml_leave_blocking_section();

  if (i == -1) {
    caml_stat_free(evs);
    uerror("kevent", Nothing);
  }

  v_res = caml_alloc(i, 0);

  while (--i >= 0) {
    value v_ev;

#ifdef USE_KEVENT
    struct kevent *ev = &evs[i];
    int flag;
    v_ev = caml_alloc_small(2,0); 
    Field(v_ev, 0) = Val_int(ev->ident);
    printf("got %d (%d).\n", ev->ident, Val_int(ev->ident));

    if( (ev->flags & EV_EOF) || (ev->flags & EV_ERROR) ){
      flag = CAML_ERROR;
    }else if( ev->filter == EVFILT_READ ){
      flag = CAML_READABLE;
    }else if( ev->filter == EVFILT_WRITE ){
      flag = CAML_WRITABLE;
    }
#elif USE_EPOLL
    struct epoll_event *ev = &evs[i];
    int flag;
    v_ev = caml_alloc_small(2,0); 
    Field(v_ev, 0) = Val_int(ev->data.fd);

    if( (ev->events & EPOLLERR) || (ev->events & EPOLLHUP) ){
      flag = CAML_ERROR;
    }else if( ev->events == EPOLLIN ){
      flag = CAML_READABLE;
    }else if( ev->events == EPOLLOUT ){
      flag = CAML_WRITABLE;
    }
#endif

    Field(v_ev, 1) = Val_int( flag );
    Store_field(v_res, i, v_ev);
  }
  caml_stat_free(evs);
  //  POUT;
  CAMLreturn(v_res);
}

CAMLprim value my_ev_add(value pfd, value v_fd, value v_flags){
  CAMLparam0();
  int flag = 0, i = Wosize_val(v_flags);
  int fd = Int_val(v_fd);
#ifdef USE_KEVENT
  struct kevent kev;
  while (--i >= 0){
    printf("%d: %d\n", i, Field(v_flags, i));
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      EV_SET( &kev, Int_val(fd), EVFILT_READ, EV_ADD, 0, 0, NULL );
      break;
    case CAML_WRITABLE:
      EV_SET( &kev, Int_val(fd), EVFILT_WRITE, EV_ADD, 0, 0, NULL );
      break;
    default:
      continue;
    }
    if(kevent(Int_val(pfd), &kev, 1, NULL, 0, NULL)){ // buggy here, Invalid Argument.
      perror("kevents.c: L184");
    }
  }
#elif USE_EPOLL
  struct epoll_event kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, ADD );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, ADD );
      break;
    default:
      continue;
    }
    epoll_ctl(Int_val(pfd), EPOLL_CTL_ADD, fd, &kev);
  }
#endif
  CAMLreturn(Val_unit);
}

CAMLprim value my_ev_mod(value pfd, value v_fd, value v_flags){
  CAMLparam0();
  int flag = 0, i = Wosize_val(v_flags);
  int fd = Int_val(v_fd);
#ifdef USE_KEVENT
  struct kevent kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, MOD );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, MOD );
      break;
    default:
      continue;
    }
    if(kevent(Int_val(pfd), &kev, 1, NULL, 0, NULL) < 0){
      printf("some error");
    }
  }
#elif USE_EPOLL
  struct epoll_event kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, MOD );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, MOD );
      break;
    default:
      continue;
    }
    epoll_ctl(Int_val(pfd), EPOLL_CTL_MOD, fd, &kev);
  }
#endif
  POUT;
  CAMLreturn(Val_unit);
}

CAMLprim value my_ev_del(value pfd, value v_fd, value v_flags){
  CAMLparam0();
  int flag = 0, i = Wosize_val(v_flags);
  int fd = Int_val(v_fd);
#ifdef USE_KEVENT
  struct kevent kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, DEL );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, DEL );
      break;
    default:
      continue;
    }
    kevent(Int_val(pfd), &kev, 1, NULL, 0, NULL);
  }
#elif USE_EPOLL
  struct epoll_event kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, DEL );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, DEL );
      break;
    default:
      continue;
    }
    epoll_ctl(Int_val(pfd), EPOLL_CTL_DEL, fd, &kev);
  }
#endif
  CAMLreturn(Val_unit);
}

CAMLprim value my_ev_oneshot(value pfd, value v_fd, value v_flags){
  CAMLparam0();
  int flag = 0, i = Wosize_val(v_flags);
  int fd = Int_val(v_fd);
#ifdef USE_KEVENT
  struct kevent kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE, EV_ONESHOT );
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE, EV_ONESHOT );
      break;
    default:
      continue;
    }
    kevent(Int_val(pfd), &kev, 1, NULL, 0, NULL);
  }
#elif USE_EPOLL
  struct epoll_event kev;
  while (--i >= 0){
    switch (Int_val(Field(v_flags, i))) {
    case CAML_READABLE:
      SET_EVENT( &kev, fd, READABLE|EPOLLONESHOT, MOD ); //needs EPOLL_CTL_ADD beforehand???
      break;
    case CAML_WRITABLE:
      SET_EVENT( &kev, fd, WRITABLE|EPOLLONESHOT, MOD ); //needs EPOLL_CTL_ADD beforehand???
      break;
    default:
      continue;
    }
    epoll_ctl(Int_val(pfd), EPOLL_CTL_ADD, fd, &kev); // should be EPOLL_CTL_MOD?? (TODO)
    epoll_ctl(Int_val(pfd), EPOLL_CTL_MOD, fd, &kev); // should be EPOLL_CTL_MOD?? (TODO)
  }
#endif
  CAMLreturn(Val_unit);
}

