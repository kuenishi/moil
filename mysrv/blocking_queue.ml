
type 'a t = {
  mutable tl: 'a list;
  mutable hd: 'a list;
  m: Mutex.t;
  c: Condition.t;
  mutable cap: int;
}

exception Empty
exception Not_implemented

let build len =
  {tl=[]; hd=[]; cap=len; m=Mutex.create(); c=Condition.create(); }

let create () = build 1024;;

let add x q=
  let rec try_push()=
    if q.cap=0 then begin
      Condition.wait q.c q.m;
      try_push()
    end
    else begin
      q.tl<-x::(q.tl);
      q.cap <- q.cap - 1;
      Mutex.unlock q.m;
      Condition.broadcast q.c;
    end
  in
  Mutex.lock q.m;
  try_push()

let push x q = add x q

let take q =
  let rec try_pop()=
    match q.hd, q.tl with
      | [], [] ->
	Condition.wait q.c q.m;
	try_pop();
      | [], _ ->
	q.hd <- List.rev q.tl;
	q.tl <- [];
	try_pop();
      | hd::tl, _ ->
	q.hd <- tl;
	q.cap <- q.cap + 1;
	hd;
  in
  Mutex.lock q.m;
  let r = try_pop() in
  Mutex.unlock q.m;
  Condition.broadcast q.c;
  r;;

let pop q = take q;;

let peek q = raise Not_implemented;;
let top q = peek q;;

let clear q =
  q.hd <- [];
  q.tl <- [];
  q.cap <- 1024;;

let copy q = q;;
(*  let r = create() in
  r.hd = q.hd; *)

let is_empty q =
  (List.length q.hd = 0) && (List.length q.tl = 0);;

let length q = (List.length q.hd) + (List.length q.tl);;

(* TODO: iter, fold, transfer *)

let iter _ _ = raise Not_implemented;;
let fold _ _ _ = raise Not_implemented;;
let transfer _ _ = raise Not_implemented;;

let push_all list q =
  let rec try_push()=
    if q.cap < List.length list then begin
      Condition.wait q.c q.m;
      try_push()
    end
    else begin
      q.tl <- List.rev_append list q.tl;
      q.cap <- q.cap - List.length list;
      Mutex.unlock q.m;
      Condition.broadcast q.c;
    end
  in
  Mutex.lock q.m;
  try_push()
  
