open Unix;;

type task = Fd2read of file_descr | Stop (* | Data2process of ? *)
    ;;

type tcp_server = {
  pfd : file_descr; (* poller socket *)
  lfd : file_descr; (* listener socket *)
  addr : inet_addr;   port : int;         (* listening port for listener socket *)
  rpipe : file_descr; wpipe : file_descr; (* controll pipe *)
};;

let close_all srv =
  close srv.lfd;
  close srv.pfd;
  close srv.rpipe;
  close srv.wpipe;;

let worker (srv, q, reader) =
  let rec loop() = 
(*    print_endline "worker: getting events... "; *)
    match (Blocking_queue.pop q) with
      | Fd2read(fd) ->
	  begin
	    try
	      (* The `reader` should do: 
		 1. read request from fd, 2. make response,
		 3. write back to fd, 4. put it again into pfd / or close it *)
	      let r = reader fd in
		begin match r with
		  | `Close ->
		      Unix.close fd; loop();
		  | `Continue -> 
		      Events.add srv.pfd fd (Array.of_list [Events.READABLE]); loop();
		  | _ -> loop(); (* unexpected; need log output *)
		end;
	    with 
	      | (Unix.Unix_error(error, msg, _))->
		  Printf.printf "%s (tcp_server.ml): %s\n" msg (error_message error);
		  loop();
	      | _ -> (* server should log this out and continue; *)
		  print_endline "unknown error.";
	  end;
      | Stop -> ()
  in
    loop();;

let rec poll q srv =
  let evs = Events.wait srv.pfd 16 10 in
(*  Printf.printf "poller: %d events." (Array.length evs); *)
  proc_fd q srv (Array.to_list evs)
and proc_fd q srv = function
  | [] -> poll q srv;
  | (fd,_)::fds when fd = srv.lfd -> (* new connection *)
    let (new_fd, _) = accept fd in
    Unix.set_nonblock new_fd;
    Blocking_queue.push (Fd2read(new_fd)) q;     (* not reading data from new_fd here *)
    proc_fd q srv fds;
  | (fd,_)::fds when fd = srv.rpipe -> (* stop command *)
    let buf = " " in
    let _ = read srv.rpipe buf 0 1 in
    close_all srv;
  | (fd,_)::fds -> (* new payload has come *)
    (* and then push the payload into q *)
    Events.del srv.pfd fd (Array.of_list [Events.READABLE]);
    Blocking_queue.push (Fd2read(fd)) q;
    proc_fd q srv fds;;

let poller (srv,q,n) =
  let _ = poll q srv in
  let rec breaker = function
    |0-> ();
    |i-> 
      let _ = Blocking_queue.push Stop q in
      breaker (i-1)
  in breaker n

(* IN: IP address, port, dispatcher, do_daemonize *)
(* OUT: none *)
(* dispatcher : (string * (string -> )) list, do_daemonize : bool *)
let start addr port dispatcher =
  let q = Blocking_queue.create() in
  let pfd = Events.create() in
  let rpipe,wpipe = Unix.pipe() in
  let saddr = ADDR_INET(addr, port) in
  let lfd = Unix.socket PF_INET SOCK_STREAM 0 in
  let _ = Unix.setsockopt lfd SO_REUSEADDR in
  bind lfd saddr;
  listen lfd 2828;
  Events.add pfd lfd (Array.of_list [Events.READABLE]);
  Events.add pfd rpipe (Array.of_list [Events.READABLE]);
  let srv = {pfd=pfd; addr=addr; port=port; lfd=lfd; rpipe=rpipe; wpipe=wpipe; } in
  let tids = Thread_pool.create worker (srv,q,dispatcher) 10 in
  let tid = Thread.create poller (srv, q, 10) in 
(*  let tids, tid  = `Hoge, 23 in  *)
(*  print_endline "tcp_server successfully started"; *)
  (tid, tids, wpipe);;

let stop (tid, tids, wpipe) = 
  let _ = Unix.single_write wpipe " " 0 1 in
  Thread_pool.join tids;
  Thread.join tid;
(*  print_endline "tcp_server successfully stopped."; *) ;
