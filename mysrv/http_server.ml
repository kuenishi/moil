
(* type http_server = { tcp_srv : Tcp_server.tcp_server; };; *)

let start addr port mycgi =
  let dispatcher x = mycgi() in
  Tcp_server.start addr port dispatcher

let stop srv =
  Tcp_server.stop srv
