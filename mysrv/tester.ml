open OUnit;;

let hello _ =
  (*assert_string (Events.env());*)
  "error!" @? (String.length (Events.env()) > 0 )

(* kevent, Events test *)
let create _ =
  let pfd = Events.create() in
  "create" @? (Unix.close pfd = ())

let events_setup funcs =
  let pfd = Events.create() in
  let r,w = Unix.pipe() in
  List.iter (fun f -> f pfd r w ) funcs;
  Unix.close r;
  Unix.close w;
  Unix.close pfd;;

let add _ =
  let f pfd r w =
    Events.add pfd r (Array.of_list [Events.READABLE; Events.ERROR]);
    Events.add pfd w (Array.of_list [Events.WRITABLE; Events.ERROR])
  in events_setup [f];
  "add" @? true;;

let del _ =
  let f pfd r w =
    Events.add pfd r (Array.of_list [Events.READABLE; Events.ERROR]);
    Events.add pfd w (Array.of_list [Events.WRITABLE; Events.ERROR]) in
  let g pfd r w =
    Events.del pfd r (Array.of_list [Events.READABLE; Events.ERROR]);
    Events.del pfd w (Array.of_list [Events.WRITABLE; Events.ERROR]) in
  events_setup [f; g];
  "add" @? true;;

let oneshot _ =
  let f pfd r w =
    Events.oneshot pfd r (Array.of_list [Events.READABLE; Events.ERROR])
  in
  let g pfd r w =
    let bytes_written = Unix.write w " " 0 1 in
    "written" @? (bytes_written > 0);
    let evs = Events.wait pfd 16 10 in
    "1 events got?" @? (1 = Array.length evs);
    let (fd, flg) = Array.get evs 0 in
    "is a new fd?" @? ( fd = r );
    "proper flag?" @? (flg == Events.READABLE)
  in    
  let j pfd r w =
    let bytes_written = Unix.write w " " 0 1 in
    "written" @? (0 < bytes_written);
    let evs = Events.wait pfd 16 10 in
    "0 events got?" @? (0 = Array.length evs) (* this doesn't work in linux and *BSD *)
  in
  events_setup [f; g; j];
  "oneshot" @? true;;

(* event polling test by using pipe fds *)
let poll _ =
  let f pfd r w =
    Events.add pfd r (Array.of_list [Events.READABLE; Events.ERROR])
  in
  let g pfd r w =
    let bytes_written = Unix.write w " " 0 1 in
    "written" @? (bytes_written > 0);
    let evs = Events.wait pfd 16 10 in
    "1 events got?" @? (1 = Array.length evs);
    let (fd, flg) = Array.get evs 0 in
    "is a new fd?" @? ( fd = r );
    "proper flag?" @? (flg == Events.READABLE)
  in
  let h pfd r w =
    let buf = "h" in
    let bytes_read = Unix.read r buf 0 1 in
    "good len?" @? (bytes_read = 1);
    "well read?" @? ( buf = " " );
  in
  events_setup [f; g; h];
  let i pfd r w =
    Events.del pfd r (Array.of_list [Events.READABLE; Events.ERROR])
  in
  let j pfd r w =
    let bytes_written = Unix.write w " " 0 1 in
    "written" @? (0 < bytes_written);
    let evs = Events.wait pfd 16 10 in
    "0 events got?" @? (0 = Array.length evs)
(*    "empty list?" @? ([] == evs) *)
  in
  events_setup [f; g; h; i; j];;

let event_suite = "events" >::: 
  [ "hello" >:: hello;
    "creat" >:: create;
    "add" >:: add;
    "del" >:: del;
    "oneshot" >:: oneshot;
    "poll" >:: poll;
  ]

(* blocking queue suite *)
let bq_test _ =
  let q = Blocking_queue.create() in
  let i,j = 234, 2345 in
  Blocking_queue.push i q;
  Blocking_queue.push j q;
  "length check" @? (Blocking_queue.length q = 2);
  "all push and pop" @? ( Blocking_queue.pop q = i );
  "all push and pop 2" @? ( Blocking_queue.pop q = j );;

let bq_suite = "blocking_queue" >:::
  [ "bq_test" >:: bq_test ]

(* Run the tests in test suite *)
let _ = OUnit.run_test_tt_main
  (TestList [event_suite;
	     bq_suite;
	     Tcp_server_test.suite() ]);;
(* Construct the test suite, merging all test suite *)

