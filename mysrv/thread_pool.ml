
let create func argv n =
  List.map (fun _ -> Thread.create func argv) (Array.to_list (Array.make n 0));;

let join tids =
  List.iter (fun tid -> (* print_endline "waiting for threads"; *) Thread.join tid) tids;;
