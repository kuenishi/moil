open OUnit;;

(* tcp_server suite *)
let start_stop _ =
(*  todo "start and stopping of tcp_server "  *)
  let nop fd = Unix.close fd; `Close in
  let server = Tcp_server.start Unix.inet_addr_any 8901 nop in
    Tcp_server.stop server;
    " - " @? true ;;

let echo fd =
  let buf = " " in
  let bytes_read = Unix.read fd buf 0 1 in
    match bytes_read, buf with
      |1,"\\0"-> `Close;
      |1,_ -> let _ = Unix.write fd buf 0 1 in `Continue;
      |_,_ -> `Close; (*error*)
;;

let read_all fd buf len =
  let rec r offs l =
    if l > 0 then
      let bytes_read = Unix.read fd buf offs l in
	r (offs+bytes_read) (l-bytes_read)
    else offs
  in
    r 0 len;;

(* just send "asdf" and check "asdf" is echoed. *)
let query _ =
  let port = 8901 in
  let server = Tcp_server.start Unix.inet_addr_any port echo in
  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
    Unix.connect sock (Unix.ADDR_INET(Unix.inet_addr_loopback, port));
    let i = Unix.write sock "asdf" 0 4 in
      "4 bytes written?" @? (i=4);
      let buf = "0000" in
      let j = read_all sock buf 4 in
	"4 bytes read?" @? (j=4);
	"valid byte read?" @? (buf = "asdf");
	Unix.close sock;
	Tcp_server.stop server;;

let two_clients _ =
  let port = 8901 in
  let server = Tcp_server.start Unix.inet_addr_any port echo in
  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  let sock2 = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
    Unix.connect sock (Unix.ADDR_INET(Unix.inet_addr_loopback, port));
    Unix.connect sock2 (Unix.ADDR_INET(Unix.inet_addr_loopback, port));
    let i = Unix.write sock "asdf" 0 4 in
    let i2 = Unix.write sock2 "asdf" 0 4 in
      "4 bytes written?" @? (i=4);
      "4 bytes written?" @? (i2=4);
      let buf = "0000" in
      let buf2 = "0000" in
      let j2 = read_all sock2 buf2 4 in
	"4 bytes read?" @? (j2=4);
	"valid byte read?" @? (buf2 = "asdf");
	let j = read_all sock buf 4 in
	  "4 bytes read?" @? (j=4);
	  "valid byte read?" @? (buf = "asdf");
	  Unix.close sock;
	  Unix.close sock2;
	  Tcp_server.stop server;;

let get_many n f =
  let rec repeat m list =
    if m > 0 then
      repeat (m-1) ((f())::list)
    else
      list
  in
    repeat n [];;

let two_many_clients n _ =
  let port = 8901 in
  let server = Tcp_server.start Unix.inet_addr_any port echo in
  let sockets = get_many n (fun ()-> Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0) in
    List.iter (fun s -> Unix.connect s (Unix.ADDR_INET(Unix.inet_addr_loopback, port))) sockets;
(*    List.iter (fun s -> let i = Unix.write s "a" 0 1 in "byte written?" @? (i = 1)) sockets; *)
(*    todo "reading many sockets at once"; *)
    (* reading many sockets at once will be some problem *)
    (*    List.iter (fun s -> let buf = " " in
	  let i = read_all s buf 1 in
	  "byte read?" @? (i = 1);
	  "valid data?" @? (buf = "a") ) sockets; *)
    List.iter (fun s -> Unix.close s) sockets;
    Tcp_server.stop server;;
    
let suite() = "server" >:::
  [ "start_stop" >:: start_stop;
    "query" >:: query;
    "two_clients" >:: two_clients;
    "two_many_clients" >:: two_many_clients 10;
  ]
