open Unix

type flag = READABLE | WRITABLE | ERROR (* with sexp, bin_io *)

(*
  IN epoll(7),  {EPOLL_CTL_ADD, EPOLL_CTL_MOD, EPOLL_CTL_DEL}
               x{EPOLLIN EPOLLOUT, EPOLLRDHUP, EPOLLHUP, EPOLLET, EEPOLLONESHOT}
  IN kevent(2), {EV_ADD, EV_ENABLE, EV_DISABLE, EV_DELETE, EV_ONESHOT, EV_CLEAR, EV_EOF, EV_ERROR }
               x{EVFILT_READ, EVFILT_WRITE, EVFILT_AIO, EVFILT_VNODE, EVFILT_PROC, EVFILT_SIGNAL, EVFILT_TIMER}

  here, we adopt only these flags as primitivs:
   { Readable, Writable, Error } x { Add, Del, Mod, Oneshot }
   vnode, proc, signal in kevent(2) is not supported. you can add ^^;
   edge-trigger in epoll(7) is supported by default it works like EPOLLET
  (while linux's default ls level-trigger, because kevent is edge-trigger by default).
*)
val create : unit -> file_descr
val add :     pfd:file_descr -> fd:file_descr -> flag array -> unit
val modify :  pfd:file_descr -> fd:file_descr -> flag array -> unit
val del :     pfd:file_descr -> fd:file_descr -> flag array -> unit
val oneshot : pfd:file_descr -> fd:file_descr -> flag array -> unit
val wait :    pfd:file_descr -> maxevents:int -> timeout:int -> (file_descr * flag) array
  
val hello : string -> unit
val env : unit -> string
